<?php
// теги в HEAD для виведення сторінки
if (!isset($title)) {$title='';} // тег TITLE 
if (!isset($msg_h1)) {$msg_h1=$title;} // заголовок в <H1>
if (!isset($msg_modul)) {$msg_modul='';} // інф. модуля
if (!isset($msg_enter)) {$msg_enter='';} 
if (!isset($msg_cat)) {$msg_cat='';} 
if (!isset($msg_info)) {$msg_info='';} // різні повідомлення
if (!isset($now_page)) {$now_page=0;}


/////////////////////////////////////////////////////////////////
// заголовок сторінки 
$msg_header = <<< EOHEAD
<!DOCTYPE html>
<html><head>
<title>$title</title>
<meta charset="utf-8">
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<!--[if lt IE 9]><script src="js/html5shiv.js"></script><![endif]-->
<script type="text/javascript" src="js/prettify.js"></script>
<!-- PRETTIFY -->
<script type="text/javascript" src="js/kickstart.js"></script>
<!-- KICKSTART -->
<script type="text/javascript" src="{$path_cssk}img/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
<!-- KICKSTART -->
<link rel="stylesheet" type="text/css" href="{$path_cssk}kickstart.css" media="all" />
<!-- KICKSTART -->
<link rel="stylesheet" type="text/css" href="{$path_css}design.css" media="all" />
<!-- CUSTOM STYLES -->
<!--[if lt IE 10]>
<link rel="stylesheet" type="text/css" href="{$path_cssk}kickstartie.css" media="all" />
<![endif]-->
</head><body><a id="top-of-page"></a>
<div id="wrap" class="site-wrap clearfix">
EOHEAD;

/////////////////////////////////////////////////////////////////
// формування сторінки
$msg_body = <<< EOBODY
<div class="col_12 d0 left">
$msg_cat
</div>
<div class="col_12 d0 right">
$msg_enter
</div>
<div class="col_12 d0 center">
<h1>$msg_h1</h1>
<div  class="msg d0">$msg_info</div>
$msg_modul
</div>
EOBODY;

/////////////////////////////////////////////////////////////////
// кінець сторінки
$msg_footer = <<< EOFOOT
<div class="clear"></div>
</div> 
</body>
</html>
EOFOOT;
//Copyright 2018




?>