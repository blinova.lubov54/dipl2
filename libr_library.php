<?php

/////////////////////////////
// з'єднання з БД
function open_db($db_host, $db_user, $db_pass, $db_name)
{
  $global_dbh = @mysqli_connect($db_host, $db_user, $db_pass, $db_name)
   or die("Немає з'єднання з БД");

  return $global_dbh;
}


/////////////////////////////
// Функція перевірки введених значень
function check($perem)
{
   if ($perem)
   { // не порожня змінна
    $perem = trim($perem);
    $perem = htmlspecialchars((stripslashes($perem)), ENT_QUOTES);
   }
  return($perem);
}


 /////////////////////////////
// відрізаємо великий рядок $value до кількості символів $n (без ... - якщо $end=false)
function string_cut_full($value,$n,$end)
{
 if (strlen($value)>$n)
 {
  $pvalue = strpos($value,' ',$n);
  if ($pvalue>0 && $pvalue<strlen($value))
  {
   $value = substr($value,0,$pvalue);
   if ($end)
   {
    $value .= "&nbsp;...";
   }
  }
 }
 return($value);
}

////////////////////////////////////////////////
// генерування паролю
function genpass()
{
 $n = 6;
 mt_srand( (double)microtime()*1000000);
 $charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
 $length = strlen($charset);
 $msg = ""; 
 for ($i=0;$i<$n;$i++)
 {
  $position = mt_rand(0,$length-1);
  $msg .= $charset[$position];
 }
 return($msg);
}

?>