<?php

// виведення графіків по даних трансформатора
$is_ok = true; /// 

$id_grp=0;
$id_t = 0;
$id_n = 0;
$name_grp="";
$name_t = "";
$name_n = "";
$select_name = "Список трансформаторів";
$select_id = "grp";
$msg_modul = "";         
$msg_select = "";
$select_dop="";

if ($mylevel==4)
{ // РЕМ - тільки своїх
  $umova = " WHERE idrem=$myid ";
}
else
{ // 
  $umova = "";
}

// зчитуємо параметри по гет, що прийшли з форм селект
 if (isset($_POST) && isset($_POST['grp']) && $_POST['grp']!='')
{
 $id_grp=$_POST['grp'];
 settype($id_grp, "integer");
} 
else
if (isset($_GET) && isset($_GET['grp']) && $_GET['grp']!='')
{
 $id_grp=$_GET['grp'];
 settype($id_grp, "integer");
}


 if (isset($_POST) && isset($_POST['t']) && $_POST['t']!='')
{
 $id_t=$_POST['t'];
 settype($id_t, "integer");
} 
else
if (isset($_GET) && isset($_GET['t']) && $_GET['t']!='')
{
 $id_t=$_GET['t'];
 settype($id_t, "integer");
}

 if (isset($_POST) && isset($_POST['n']) && $_POST['n']!='')
{
 $id_n=$_POST['n'];
 settype($id_n, "integer");
} 
else
if (isset($_GET) && isset($_GET['n']) && $_GET['n']!='')
{
 $id_n=$_GET['n'];
 settype($id_n, "integer");
}


// якщо наттиснута одна з кнопок "змінити", зкидаємо той параметр для нового вибору
if (isset($_POST))
{
 if (isset($_POST['change']) )
 {
  $id_grp = 0;
 }
 else
 if (isset($_POST['changen']) )
 {
  $id_n = 0;
 }
 else
 if (isset($_POST['changet']) )
 {
  $id_t = 0;
 }
}


// записуємо додаткові параметри в форми селект
if ($id_grp>0)
{
 $select_dop.="&grp=$id_grp";
}
if ($id_t>0)
{
 $select_dop.="&t=$id_t";
}
if ($id_n>0)
{
 $select_dop.="&n=$id_n";
}


 // формуємо список трансформаторів
$myquery = "SELECT id, name FROM trans $umova ORDER BY name" ; 

$result = mysqli_query($mylink,$myquery);
if ($result && mysqli_num_rows($result)>0 )
{

 $mas_selest = array();
 $maxs=0;
  while ($row = mysqli_fetch_row($result))
  { 
   $maxs++;
   $mas_selest[$maxs] = array($row[1],$row[0]); 
   if ($id_grp==$row[0])
   { // вже обрано групу - зберігаємо назву
    $name_grp = $row[1];
   }
  } 
 
 require($path_tpl."select.php"); // формування списку 
  
}

 if ($id_grp==0)
 {
  $msg_modul .= "<div class=\"col_3\">".$msg_select."</div>";
 }  
 else
 {
  $msg_modul .= "<div class=\"col_3\">"."<h3>Трансформатор: ".$name_grp."</h3>".$msg_select_change."</div>";
 }


  // тип величини
  $select_name = "Покажчик";
  $select_id = "n";
  $formname1 = "formdatan1";
  $formname2 = "formdatan2";
  $select_but = "changen";

  // тип величини
$myquery = "SELECT id, name FROM typesdata ORDER BY name" ; 

$result = mysqli_query($mylink,$myquery);
if ($result && mysqli_num_rows($result)>0 )
{

 $mas_selest = array();
 $maxs=0;
  while ($row = mysqli_fetch_row($result))
  { 
   $maxs++;
   $mas_selest[$maxs] = array($row[1],$row[0]); 
   if ($id_n==$row[0])
   { // вже обрано групу - зберігаємо назву
    $name_n = $row[1];
   }
  } 
 
  require($path_tpl."select.php"); // формування списку 
  
 } 
 if ($id_n==0)
 {
  $msg_modul .= "<div class=\"col_6\">".$msg_select."</div>";
 }  
 else
 {
  $msg_modul .= "<div class=\"col_6\">"."<h3>Покажчик: ".$name_n."</h3>".$msg_select_change."</div>";
 }
 

  
  // період
  $select_name = "Дата";
  $select_id = "t";
  $formname1 = "formdatat1";
  $formname2 = "formdatat2";
  $select_but = "changet";

  $data1 = 1;
  $data2 = date("d");
  $mon = date("m");
  $maxs=0;
  for ($s=$data2-1;$s>=$data1;$s--)
  { 
   $maxs++;
   $mas_selest[$maxs] = array($s.".".$mon,$s); 
   if ($id_t==$s)
   { // вже обрано  - зберігаємо назву
    $name_t = $s.".".$mon;
   }
  } 
  require($path_tpl."select.php"); // формування списку 
 if ($id_t==0)
 {
  $msg_modul .= "<div class=\"col_3\">".$msg_select."</div>";
 }  
 else
 {
  $msg_modul .= "<div class=\"col_3\">"."<h3>Період: ".$name_t."</h3>".$msg_select_change."</div>";
 }
 
  
 if ($id_n>0 && $id_t>0 && $id_grp>0)
 { // зчитуємо дані і будуємо графік
//   require($path_tpl."datas.php");  
 // $img = "<img src=\"gr.php\">";
  $msg_modul .= "<div class=\"col_12\"><img src=\"gr.php?l=$mylevel$select_dop\"></div>";

 require($path_tpl."datas.php"); // формування  
 }
  
  

?>